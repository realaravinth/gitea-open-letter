# Gitea Ltd confirms its takeover of the Gitea project

On 28 October, members of the Gitea Community published the [Gitea Open Letter](https://gitea-open-letter.coding.social/) demanding restitution of the Gitea project after [the takeover announced on 25 October](https://blog.gitea.io/2022/10/open-source-sustainment-and-the-future-of-gitea/).

After discussions via diplomatic channels about the Gitea Open Letter, Gitea Ltd decided to ignore the demands [and further confirmed they now are in total control of the Gitea project](https://blog.gitea.io/2022/10/a-message-from-lunny-on-gitea-ltd.-and-the-gitea-project/) in a blog post signed by two shareholders, Lunny and techknowlogick on 30 October.

This unfortunately concludes the Gitea Open Letter has failed and there is **no alternative but forking the project under a new name**, with a healthy democratic governance. Exactly as it was before 25 October in the Gitea project. But this time in the context of an incorporated non-profit that provides a legal framework.
